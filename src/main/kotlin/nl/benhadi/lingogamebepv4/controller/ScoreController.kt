package nl.benhadi.lingogamebepv4.controller

import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.game.exception.GameNotFoundException
import nl.benhadi.lingogamebepv4.game.service.GameService
import nl.benhadi.lingogamebepv4.score.dto.ScoreDTO
import nl.benhadi.lingogamebepv4.score.service.ScoreService
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*

@RestController
@RequestMapping("/score")
class ScoreController(private val scoreService: ScoreService,
                      private val gameService: GameService) {

    @GetMapping("{gameUuid}")
    fun getScore(@AuthenticationPrincipal account: AccountDTO,
                 @PathVariable gameUuid: UUID): ScoreDTO {

        val gameDTO = gameService.findByAccountUuidAndUuid(accountUuid = account.uuid, gameUuid = gameUuid)
                ?: throw GameNotFoundException(gameUuid)

        val amount = scoreService.calculate(gameDTO)
        return ScoreDTO(amount = amount, name = account.email, gameUuid = gameDTO.uuid)
    }

    @PatchMapping("{gameUuid}")
    fun saveScore(@AuthenticationPrincipal account: AccountDTO,
                  @PathVariable gameUuid: UUID): ScoreDTO {

        scoreService.findByGameUuid(gameUuid)?.let {
            throw ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Score has already been persisted to the database")
        }

        val gameDTO = gameService.findByAccountUuidAndUuid(accountUuid = account.uuid, gameUuid = gameUuid)
                ?: throw GameNotFoundException(gameUuid)

        return scoreService.save(gameDTO, account.email)
    }

    @GetMapping
    fun getAllScores(): List<ScoreDTO> {
        return scoreService.findAll()
    }

}