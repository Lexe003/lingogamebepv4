package nl.benhadi.lingogamebepv4.controller

import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.account.request.RegisterAccountRequest
import nl.benhadi.lingogamebepv4.account.service.AccountService
import nl.benhadi.lingogamebepv4.config.jwt.service.JwtCookieService
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class AccountController(private val jwtCookieService: JwtCookieService,
                        private val accountService: AccountService) {

    @GetMapping("/account")
    fun getAccount(@AuthenticationPrincipal account: AccountDTO): AccountDTO {
        return account
    }

    @PostMapping("/account/register")
    fun registerAccount(@RequestBody registerAccountRequest: RegisterAccountRequest ) : AccountDTO {
        when (accountService.findAccountByEmail(registerAccountRequest.username) != null) {
            true -> throw ResponseStatusException(HttpStatus.CONFLICT, "Account with email: ${registerAccountRequest.username} already taken")
            else -> return accountService.registerNewAccount(registerAccountRequest.username, registerAccountRequest.password);
        }
    }

    @DeleteMapping("/token")
    fun logout(httpServletRequest: HttpServletRequest, httpServletResponse: HttpServletResponse) {
        httpServletResponse.addCookie(jwtCookieService.createRemoveCookie(httpServletRequest))
    }

}