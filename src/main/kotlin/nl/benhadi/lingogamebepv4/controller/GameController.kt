package nl.benhadi.lingogamebepv4.controller

import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.game.request.AttemptRequest
import nl.benhadi.lingogamebepv4.game.service.GameService
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*

@RestController
@RequestMapping("/game")
class GameController(private val gameService: GameService) {

    @GetMapping
    fun initializeGame(@AuthenticationPrincipal account: AccountDTO) : GameDTO {
        return gameService.createNewGame(account)
    }

    @PostMapping("{gameUuid}/attempt")
    fun newAttempt(@AuthenticationPrincipal account: AccountDTO,
                   @PathVariable gameUuid: UUID,
                   @RequestBody attemptRequest: AttemptRequest) : GameDTO {

        val gameDTO = gameService.findByAccountUuidAndUuid(accountUuid = account.uuid, gameUuid = gameUuid)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Game with $gameUuid not found")

        return gameService.processAttempt(gameDTO, attemptRequest.guessedWord)
    }
}