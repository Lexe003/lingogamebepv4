package nl.benhadi.lingogamebepv4.controller

import nl.benhadi.lingogamebepv4.word.request.WordRequest
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.annotation.Input
import org.springframework.cloud.stream.annotation.StreamListener
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.SubscribableChannel
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId

@EnableBinding(LingoGameListener.EmailSink::class)
class LingoGameListener(private val wordService: WordService) {

    @StreamListener(EmailSink.INPUT)
    fun receive(words: List<WordRequest>, messageHeader: MessageHeaders) {
        val headerTimestamp = messageHeader[KafkaHeaders.RECEIVED_TIMESTAMP] as Long
        val messageTime = OffsetDateTime.ofInstant(Instant.ofEpochMilli(headerTimestamp), ZoneId.systemDefault())
        wordService.saveAll(words)
        println("Words received: ${words.size} on $messageTime")
    }

    interface EmailSink {

        @Input(INPUT)
        fun input(): SubscribableChannel

        companion object {
            const val INPUT = "input"
        }
    }
}

