package nl.benhadi.lingogamebepv4.role.service

import nl.benhadi.lingogamebepv4.role.dto.RoleDTO
import nl.benhadi.lingogamebepv4.role.dto.RoleMapper
import nl.benhadi.lingogamebepv4.role.entity.RoleEntity
import nl.benhadi.lingogamebepv4.role.repository.RoleRepository
import org.springframework.stereotype.Service

@Service
class RoleService(private val roleRepository: RoleRepository,
                  private val roleMapper: RoleMapper) {

    fun findRoleByName(role: String): RoleDTO {
        return roleMapper.map(roleRepository.findByName(role))
    }
}