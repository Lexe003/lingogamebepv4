package nl.benhadi.lingogamebepv4.role.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "role")
class RoleEntity(uuid: UUID? = null,
                 @Column(nullable = false) val name: String ) : BaseEntity(uuid)