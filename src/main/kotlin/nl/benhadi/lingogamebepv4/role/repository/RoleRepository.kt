package nl.benhadi.lingogamebepv4.role.repository

import nl.benhadi.lingogamebepv4.role.entity.RoleEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RoleRepository: JpaRepository<RoleEntity, UUID> {
    fun findByName(name: String): RoleEntity
}