package nl.benhadi.lingogamebepv4.role.dto

import java.util.*

data class RoleDTO(val uuid: UUID, val name: String)