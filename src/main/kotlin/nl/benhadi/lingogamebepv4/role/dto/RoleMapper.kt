package nl.benhadi.lingogamebepv4.role.dto

import nl.benhadi.lingogamebepv4.role.entity.RoleEntity
import org.springframework.stereotype.Component

@Component
class RoleMapper {

    fun map(roleEntity: RoleEntity): RoleDTO {
        return RoleDTO(
                uuid = roleEntity.id,
                name = roleEntity.name
        )
    }

    fun map(roleDTO: RoleDTO): RoleEntity {
        return RoleEntity(
                uuid = roleDTO.uuid,
                name = roleDTO.name
        )
    }
}