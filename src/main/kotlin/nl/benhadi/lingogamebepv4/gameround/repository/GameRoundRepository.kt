package nl.benhadi.lingogamebepv4.gameround.repository

import nl.benhadi.lingogamebepv4.gameround.entity.GameRoundEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface GameRoundRepository: JpaRepository<GameRoundEntity, UUID>