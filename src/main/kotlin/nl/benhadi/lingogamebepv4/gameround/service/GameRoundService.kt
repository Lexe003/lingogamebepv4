package nl.benhadi.lingogamebepv4.gameround.service

import nl.benhadi.lingogamebepv4.TOTAL_GAME_ROUNDS
import nl.benhadi.lingogamebepv4.feedback.dto.FeedbackDTO
import nl.benhadi.lingogamebepv4.feedback.service.FeedbackService
import nl.benhadi.lingogamebepv4.game.enum.Phase
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundDTO
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundMapper
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.Comparator

@Service
class GameRoundService(private val feedbackService: FeedbackService,
                       private val gameRoundMapper: GameRoundMapper,
                       private val wordService: WordService) {

    fun process(allGameRounds: List<GameRoundDTO>, currentGameRound: GameRoundDTO, guessedWord: String): List<GameRoundDTO> {

        val feedbackList = processFeedback(currentGameRound, guessedWord)

        val hasGuessedWord = currentGameRound.wordToGuess.name == guessedWord
        val gameRoundIsFinished = hasGuessedWord || feedbackList.size >= TOTAL_GAME_ROUNDS
        val updatedCurrentGameRound = currentGameRound.copy(attempts = feedbackList, canAttempt = !gameRoundIsFinished)

        val updatedGameRounds = allGameRounds.filter { gameRoundDTO -> gameRoundDTO.uuid != updatedCurrentGameRound.uuid }.toMutableList()
        updatedGameRounds.add(updatedCurrentGameRound)

        return if (hasGuessedWord) {
            processGameRound(updatedGameRounds)
        } else {
            updatedGameRounds
        }
    }

    private fun processFeedback(currentGameRound: GameRoundDTO, guessedWord: String): List<FeedbackDTO> {
        val comparator = Comparator.comparing(FeedbackDTO::creationTime)
        val lastAttempt = currentGameRound.attempts.maxWith(comparator) ?:
                throw ResponseStatusException(HttpStatus.CONFLICT, "No attempts found!")


        val feedbackDTO = feedbackService.process(
                gameRoundUuid = currentGameRound.uuid,
                guessedWord = guessedWord,
                wordToGuess = currentGameRound.wordToGuess.name,
                timeLastAttempt = lastAttempt.creationTime
                )

        val feedbackList = currentGameRound.attempts.toMutableList()
        feedbackList.add(feedbackDTO)

        return feedbackList
    }

    private fun processGameRound(allGameRounds: List<GameRoundDTO>): List<GameRoundDTO> {

        when (allGameRounds.size < 3) {
            true -> {
                val currentGameRound = allGameRounds.last()
                val phase = getPhaseByLengthGameRounds(allGameRounds.size)

                val wordToGuess = wordService.getRandomWordByPhase(phase)
                        ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Failed to load new game round, could not find word with length ${currentGameRound.wordToGuess.name.length + 1}")

                val updatedGameRounds = allGameRounds.toMutableList()
                val newGameRound = gameRoundMapper.map(currentGameRound.gameUuid, wordToGuess).let { gameRoundMapper.map(it) }
                updatedGameRounds.add(newGameRound)

                return updatedGameRounds
            }
            false -> return allGameRounds
        }
    }

    private fun getPhaseByLengthGameRounds(length: Int): Phase {
        return when (length) {
            1 -> Phase.TWO
            else -> Phase.THREE
        }
    }

}