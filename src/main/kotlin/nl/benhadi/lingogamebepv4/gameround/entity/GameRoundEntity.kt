package nl.benhadi.lingogamebepv4.gameround.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import nl.benhadi.lingogamebepv4.feedback.entity.FeedbackEntity
import nl.benhadi.lingogamebepv4.score.entity.ScoreEntity
import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "game_round")
class GameRoundEntity(uuid: UUID? = null,
                      @OneToOne val wordToGuess: WordEntity,
                      @Column(nullable = false) val gameUuid: UUID,
                      @Column(nullable = false) val canAttempt: Boolean,
                      @OneToMany(
                              mappedBy = "gameRoundUuid",
                              cascade = [CascadeType.ALL],
                              fetch = FetchType.EAGER,
                              orphanRemoval = true
                      ) val feedbacks: List<FeedbackEntity>,
                      val creationTime: OffsetDateTime = OffsetDateTime.now()
) : BaseEntity(uuid)