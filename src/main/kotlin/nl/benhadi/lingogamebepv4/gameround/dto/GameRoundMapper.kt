package nl.benhadi.lingogamebepv4.gameround.dto

import nl.benhadi.lingogamebepv4.feedback.dto.FeedbackMapper
import nl.benhadi.lingogamebepv4.gameround.entity.GameRoundEntity
import nl.benhadi.lingogamebepv4.word.dto.WordMapper
import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import org.springframework.stereotype.Component
import java.util.*

@Component
class GameRoundMapper(private val wordMapper: WordMapper,
                      private val feedbackMapper: FeedbackMapper) {

    fun map(gameRoundDTO: GameRoundDTO): GameRoundEntity {
        return GameRoundEntity(
                uuid = gameRoundDTO.uuid,
                wordToGuess = wordMapper.map(gameRoundDTO.wordToGuess),
                gameUuid = gameRoundDTO.gameUuid,
                feedbacks = gameRoundDTO.attempts.map { feedbackMapper.map(it) },
                canAttempt = gameRoundDTO.canAttempt,
                creationTime = gameRoundDTO.creationTime)
    }

    fun map(gameRoundEntity: GameRoundEntity): GameRoundDTO {
        return GameRoundDTO(
                uuid = gameRoundEntity.id,
                wordToGuess = wordMapper.map(gameRoundEntity.wordToGuess),
                gameUuid = gameRoundEntity.gameUuid,
                attempts = gameRoundEntity.feedbacks.map { feedbackMapper.map(it) },
                canAttempt = gameRoundEntity.canAttempt,
                creationTime = gameRoundEntity.creationTime)
    }

    fun map(gameUuid: UUID, wordToGuess: WordEntity): GameRoundEntity {
        val gameRoundUuid = UUID.randomUUID()
        return GameRoundEntity(
                uuid = gameRoundUuid,
                wordToGuess = wordToGuess,
                gameUuid = gameUuid,
                canAttempt = true,
                feedbacks = mutableListOf(feedbackMapper.mapInit(gameRoundUuid, wordToGuess))
        )
    }
}