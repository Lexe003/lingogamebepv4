package nl.benhadi.lingogamebepv4.gameround.dto

import nl.benhadi.lingogamebepv4.feedback.dto.FeedbackDTO
import nl.benhadi.lingogamebepv4.score.dto.ScoreDTO
import nl.benhadi.lingogamebepv4.word.dto.WordDTO
import java.time.OffsetDateTime
import java.util.*

data class GameRoundDTO(val uuid: UUID,
                        val wordToGuess: WordDTO,
                        val gameUuid: UUID,
                        var attempts: List<FeedbackDTO>,
                        var canAttempt: Boolean,
                        val creationTime: OffsetDateTime
)