package nl.benhadi.lingogamebepv4.game.repository

import nl.benhadi.lingogamebepv4.game.entity.GameEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface GameRepository : JpaRepository<GameEntity, UUID> {
    fun findByAccountUuidAndUuid(accountUuid: UUID, gameUuid: UUID) : GameEntity?
}