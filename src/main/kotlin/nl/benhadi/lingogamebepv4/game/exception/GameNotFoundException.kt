package nl.benhadi.lingogamebepv4.game.exception

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.util.*

class GameNotFoundException(gameUUID: UUID) : ResponseStatusException(HttpStatus.NOT_FOUND, "Game with uuid $gameUUID not found")