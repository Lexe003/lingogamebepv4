package nl.benhadi.lingogamebepv4.game.service

import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.game.dto.GameMapper
import nl.benhadi.lingogamebepv4.game.enum.Phase
import nl.benhadi.lingogamebepv4.game.repository.GameRepository
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundDTO
import nl.benhadi.lingogamebepv4.gameround.service.GameRoundService
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class GameService(private val gameRepository: GameRepository,
                  private val gameMapper: GameMapper,
                  private val wordService: WordService,
                  private val gameRoundService: GameRoundService) {

    fun findByAccountUuidAndUuid(accountUuid: UUID, gameUuid: UUID): GameDTO? {
        return gameRepository.findByAccountUuidAndUuid(accountUuid = accountUuid, gameUuid = gameUuid)?.let {
            gameMapper.map(it)
        }
    }

    fun save(gameDTO: GameDTO): GameDTO {
        val gameEntity = gameMapper.map(gameDTO)
        return gameRepository.save(gameEntity).let { gameMapper.map(it) }
    }

    fun createNewGame(accountDTO: AccountDTO): GameDTO {
        val wordToGuess = wordService.getRandomWordByPhase(Phase.ONE)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "No words found in the database")

        val game = gameMapper.map(accountUuid = accountDTO.uuid, wordToGuess = wordToGuess)
        return gameRepository.save(game).let { gameMapper.map(it) }
    }

    fun processAttempt(gameDto: GameDTO, guessedWord: String): GameDTO {
        val comparator = Comparator.comparing(GameRoundDTO::creationTime)
        val currentGameRound = gameDto.gameRounds.maxWith(comparator)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "No game rounds found!")

        when (!currentGameRound.canAttempt) {
            true -> throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Game ended already!")
        }

        val gameRounds = gameRoundService.process(gameDto.gameRounds, currentGameRound, guessedWord)
        return save(gameDto.copy(gameRounds = gameRounds))
    }

}