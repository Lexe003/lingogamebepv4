package nl.benhadi.lingogamebepv4.game.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import nl.benhadi.lingogamebepv4.feedback.entity.FeedbackEntity
import nl.benhadi.lingogamebepv4.game.enum.Phase
import nl.benhadi.lingogamebepv4.gameround.entity.GameRoundEntity
import nl.benhadi.lingogamebepv4.score.entity.ScoreEntity
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "game")
class GameEntity(uuid: UUID? = null,
                 @Column(nullable = false) val accountUuid: UUID,
                 @OneToMany(
                         mappedBy = "gameUuid",
                         cascade = [CascadeType.ALL],
                         fetch = FetchType.LAZY,
                         orphanRemoval = true) val gameRounds: List<GameRoundEntity>
) : BaseEntity(uuid)