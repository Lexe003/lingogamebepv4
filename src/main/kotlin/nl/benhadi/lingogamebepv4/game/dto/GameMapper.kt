package nl.benhadi.lingogamebepv4.game.dto

import nl.benhadi.lingogamebepv4.game.entity.GameEntity
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundMapper
import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import org.springframework.stereotype.Component
import java.util.*

@Component
class GameMapper(private val gameRoundMapper: GameRoundMapper) {

    fun map(gameDTO: GameDTO): GameEntity {
        return GameEntity(
                uuid = gameDTO.uuid,
                accountUuid = gameDTO.accountUuid,
                gameRounds = gameDTO.gameRounds.map { gameRoundMapper.map(it) }
        )
    }

    fun map(gameEntity: GameEntity): GameDTO {
        return GameDTO(
                uuid = gameEntity.id,
                accountUuid = gameEntity.accountUuid,
                gameRounds = gameEntity.gameRounds.map { gameRoundMapper.map(it) }
        )
    }

    fun map(accountUuid: UUID, wordToGuess: WordEntity) : GameEntity {
        val gameUuid = UUID.randomUUID()
        return GameEntity(
                uuid = gameUuid,
                accountUuid = accountUuid,
                gameRounds = mutableListOf(gameRoundMapper.map(gameUuid, wordToGuess))
        )
    }
}