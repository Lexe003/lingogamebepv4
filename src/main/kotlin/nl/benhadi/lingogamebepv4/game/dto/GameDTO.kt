package nl.benhadi.lingogamebepv4.game.dto

import java.util.*
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundDTO

data class GameDTO(val uuid: UUID,
                   val accountUuid: UUID,
                   val gameRounds: List<GameRoundDTO>)