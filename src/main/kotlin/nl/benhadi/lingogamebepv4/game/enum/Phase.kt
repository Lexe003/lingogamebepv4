package nl.benhadi.lingogamebepv4.game.enum

enum class Phase {
    ONE, TWO, THREE
}