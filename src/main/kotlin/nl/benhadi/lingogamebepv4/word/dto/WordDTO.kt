package nl.benhadi.lingogamebepv4.word.dto

import java.util.*

data class WordDTO(val uuid: UUID,
                   val name: String)