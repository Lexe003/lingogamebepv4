package nl.benhadi.lingogamebepv4.word.dto

import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import nl.benhadi.lingogamebepv4.word.request.WordRequest
import org.springframework.stereotype.Component

@Component
class WordMapper {

    fun map(wordRequest: WordRequest): WordEntity {
            return WordEntity(
                    name = wordRequest.name
            )
    }

    fun map(wordDTO: WordDTO): WordEntity {
        return WordEntity(
                uuid = wordDTO.uuid,
                name = wordDTO.name
        )
    }

    fun map(wordEntity: WordEntity): WordDTO {
        return WordDTO(
                uuid = wordEntity.id,
                name = wordEntity.name
        )
    }
}