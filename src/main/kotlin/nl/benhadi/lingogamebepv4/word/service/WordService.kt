package nl.benhadi.lingogamebepv4.word.service

import nl.benhadi.lingogamebepv4.game.enum.Phase
import nl.benhadi.lingogamebepv4.word.dto.WordDTO
import nl.benhadi.lingogamebepv4.word.dto.WordMapper
import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import nl.benhadi.lingogamebepv4.word.repository.WordRepository
import nl.benhadi.lingogamebepv4.word.request.WordRequest
import org.springframework.stereotype.Service

@Service
class WordService(private val wordRepository: WordRepository,
                  private val wordMapper: WordMapper) {

    fun findAll(): List<WordDTO> {
        return wordRepository.findAll().map { wordMapper.map(it) }
    }

    fun saveAll(incomingWords: List<WordRequest>) {
        val allWords = findAll().map { WordRequest(it.name) }
        val filteredWords = incomingWords.distinct().filter {  !allWords.contains(it) }.map { wordMapper.map(it) }
        wordRepository.saveAll(filteredWords)
    }

    fun wordExists(name: String) : Boolean {
        return wordRepository.existsWordEntityByName(name)
    }

    fun getRandomWordByPhase(phase: Phase): WordEntity? {
        return when (phase) {
            Phase.ONE -> wordRepository.findRandomByLength(5).first()
            Phase.TWO -> wordRepository.findRandomByLength(6).first()
            Phase.THREE -> wordRepository.findRandomByLength(7).first()
        }
    }
}