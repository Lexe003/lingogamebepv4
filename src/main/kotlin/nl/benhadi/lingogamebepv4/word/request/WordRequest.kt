package nl.benhadi.lingogamebepv4.word.request

import lombok.Data
import java.lang.IllegalArgumentException

@Data
data class WordRequest(val name: String) {

    init {
        if (name.isEmpty() || !name.matches(Regex("^[a-z]{5,7}$"))){
            throw IllegalArgumentException("$name is not a valid word for lingogame")
        }
    }

}