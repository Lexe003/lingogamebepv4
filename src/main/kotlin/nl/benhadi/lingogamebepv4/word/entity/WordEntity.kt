package nl.benhadi.lingogamebepv4.word.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import java.lang.IllegalArgumentException
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "word")
class WordEntity(uuid: UUID? = null,
                 @Column(nullable = false, unique = true) val name: String
) : BaseEntity(uuid) {

    init {
        if (name.isEmpty() || !name.matches(Regex("^[a-z]{5,7}$"))){
            throw IllegalArgumentException("$name is not a valid word for lingogame")
        }
    }
}