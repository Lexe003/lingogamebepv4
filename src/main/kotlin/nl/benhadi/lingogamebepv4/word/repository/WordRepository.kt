package nl.benhadi.lingogamebepv4.word.repository

import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface WordRepository : JpaRepository<WordEntity, UUID> {
    fun existsWordEntityByName(name: String): Boolean
    @Query(
            value = "select * from word where length(name) = ?1 ORDER BY random() LIMIT 1",
            nativeQuery = true)
    fun findRandomByLength(length: Int): List<WordEntity>
}