package nl.benhadi.lingogamebepv4.score.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "score")
class ScoreEntity(uuid: UUID? = null,
                  @Column(nullable = false) val amount: Int,
                  @Column(nullable = false) val gameUuid: UUID,
                  @Column(nullable = false) val name: String
) : BaseEntity(uuid)