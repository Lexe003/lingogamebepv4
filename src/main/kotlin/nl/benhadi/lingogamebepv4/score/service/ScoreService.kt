package nl.benhadi.lingogamebepv4.score.service

import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.score.dto.ScoreDTO
import nl.benhadi.lingogamebepv4.score.dto.ScoreMapper
import nl.benhadi.lingogamebepv4.score.entity.ScoreEntity
import nl.benhadi.lingogamebepv4.score.repository.ScoreRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class ScoreService(private val scoreRepository: ScoreRepository,
                    private val scoreMapper: ScoreMapper) {

    fun calculate(gameDTO: GameDTO): Int {
        var score = 0
        gameDTO.gameRounds.forEach {
            when (it.canAttempt) {
                true -> score += 0
                else -> {
                    val totalAttempts = it.attempts.size - 1
                    val lastAttempt = it.attempts.last()
                    val hasGuessedCorrectly = lastAttempt.guessedWord == it.wordToGuess.name

                    if (hasGuessedCorrectly) {
                        score += 50 - (totalAttempts * 10) + 10
                    }
                }
            }
        }

        return score
    }

    fun findAll(): List<ScoreDTO> {
        return scoreRepository.findAll().map { scoreMapper.map(it) }
    }

    fun findByGameUuid(gameUuid: UUID): ScoreDTO? {
        return scoreRepository.findByGameUuid(gameUuid)?.let { scoreMapper.map(it) }
    }

    fun save(gameDTO: GameDTO, email: String): ScoreDTO {
        scoreRepository.save(ScoreEntity(amount = calculate(gameDTO), gameUuid = gameDTO.uuid, name = email)).let {
            return scoreMapper.map(it)
        }
    }
}