package nl.benhadi.lingogamebepv4.score.repository

import nl.benhadi.lingogamebepv4.score.entity.ScoreEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ScoreRepository: JpaRepository<ScoreEntity, UUID> {
    fun findByGameUuid(gameUuid: UUID): ScoreEntity?
}