package nl.benhadi.lingogamebepv4.score.dto

import com.fasterxml.jackson.annotation.JsonInclude
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ScoreDTO(val uuid: UUID? = null,
                    val amount: Int,
                    val gameUuid: UUID,
                    val name: String)