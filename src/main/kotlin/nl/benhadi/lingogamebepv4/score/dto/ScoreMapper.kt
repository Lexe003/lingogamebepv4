package nl.benhadi.lingogamebepv4.score.dto

import nl.benhadi.lingogamebepv4.score.entity.ScoreEntity
import org.springframework.stereotype.Component

@Component
class ScoreMapper {

    fun map(scoreEntity: ScoreEntity) : ScoreDTO {
        return ScoreDTO(
                uuid = scoreEntity.id,
                amount = scoreEntity.amount,
                gameUuid = scoreEntity.gameUuid,
                name = scoreEntity.name
        )
    }
}