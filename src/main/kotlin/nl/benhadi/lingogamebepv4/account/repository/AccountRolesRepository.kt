package nl.benhadi.lingogamebepv4.account.repository

import nl.benhadi.lingogamebepv4.account.entity.AccountRolesEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface AccountRolesRepository: JpaRepository<AccountRolesEntity, UUID> {
    fun findByAccountUuidAndRole_Name(accountUuid: UUID, roleName: String): AccountRolesEntity?
    fun deleteAccountRolesEntitiesByAccountUuid(accountUuid: UUID)
}