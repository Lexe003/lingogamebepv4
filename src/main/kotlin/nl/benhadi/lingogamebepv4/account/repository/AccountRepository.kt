package nl.benhadi.lingogamebepv4.account.repository

import nl.benhadi.lingogamebepv4.account.entity.AccountEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AccountRepository : JpaRepository<AccountEntity, UUID> {
    fun findByEmail(email: String): AccountEntity?
}