package nl.benhadi.lingogamebepv4.account.service

import nl.benhadi.lingogamebepv4.account.dto.AccountRolesDTO
import nl.benhadi.lingogamebepv4.account.dto.AccountRolesMapper
import nl.benhadi.lingogamebepv4.account.repository.AccountRolesRepository
import nl.benhadi.lingogamebepv4.role.dto.RoleDTO
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.persistence.PreRemove

@Service
class AccountRolesService(private val accountRolesRepository: AccountRolesRepository,
                          private val accountRolesMapper: AccountRolesMapper) {

    fun findAccountRolesByUserUuidAndRoleName(userUuid: UUID, roleName: String): AccountRolesDTO? {
        return accountRolesRepository.findByAccountUuidAndRole_Name(userUuid, roleName)?.let {
            accountRolesMapper.map(it)
        }
    }

    @PreRemove
    @Transactional
    fun deleteByAccountUuid(accountUUID: UUID) {
        accountRolesRepository.deleteAccountRolesEntitiesByAccountUuid(accountUUID)
    }

    fun saveAccountRole(accountUUID: UUID, role: RoleDTO) : AccountRolesDTO {
        val accountRoleEntity = accountRolesMapper.map(accountUUID, role)
        return accountRolesMapper.map(accountRolesRepository.save(accountRoleEntity))
    }
}