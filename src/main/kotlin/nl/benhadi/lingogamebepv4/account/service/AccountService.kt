package nl.benhadi.lingogamebepv4.account.service

import nl.benhadi.lingogamebepv4.PLAYER
import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.account.dto.AccountMapper
import nl.benhadi.lingogamebepv4.account.dto.AccountRolesDTO
import nl.benhadi.lingogamebepv4.account.dto.AccountRolesMapper
import nl.benhadi.lingogamebepv4.account.repository.AccountRepository
import nl.benhadi.lingogamebepv4.game.dto.GameMapper
import nl.benhadi.lingogamebepv4.role.dto.RoleDTO
import nl.benhadi.lingogamebepv4.role.service.RoleService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class AccountService(private val accountRepository: AccountRepository,
                     private val accountMapper: AccountMapper,
                     private val accountRolesService: AccountRolesService,
                     private val accountRolesMapper: AccountRolesMapper,
                     private val roleService: RoleService,
                     private val gameMapper: GameMapper) : UserDetailsService {

    @Transactional(readOnly = true)
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(email: String): UserDetails {
        val user = accountRepository.findByEmail(email) ?: throw UsernameNotFoundException(email)
        return AccountDTO(uuid = user.id,
                email = user.email,
                password = user.password,
                accountRoles = user.accountRolesEntity.map {
                    accountRolesMapper.map(it)
                },
                games = user.games.map { gameMapper.map(it) })
    }

    fun registerNewAccount(email: String, password: String): AccountDTO {
        val roleDTO = roleService.findRoleByName(PLAYER)
        val newAccount = accountMapper.map(
                email = email,
                password = hashPassword(password),
                gameEntities = emptyList(),
                roles = emptyList())

        val createdAccountDTO = accountMapper.map(accountRepository.save(newAccount))
        val mappedAccountRoles = mapAndSaveAccountRoles(roleDTO, createdAccountDTO.uuid)
        val mappedAccountWithNewRoles = accountMapper.map(createdAccountDTO.copy(accountRoles = mappedAccountRoles))

        return accountMapper.map(accountRepository.save(mappedAccountWithNewRoles))
    }

    fun findAccountByEmail(email: String): AccountDTO? {
        return accountRepository.findByEmail(email)?.let {
            accountMapper.map(it)
        }
    }

    private fun hashPassword(password: String): String {
        return BCrypt.hashpw(password, BCrypt.gensalt())
    }

    private fun mapAndSaveAccountRoles(role: RoleDTO, accountUuid: UUID): List<AccountRolesDTO> {
        return arrayListOf(accountRolesService.saveAccountRole(accountUuid, role))
    }
}