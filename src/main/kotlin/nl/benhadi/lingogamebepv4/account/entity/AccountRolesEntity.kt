package nl.benhadi.lingogamebepv4.account.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import nl.benhadi.lingogamebepv4.role.entity.RoleEntity
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "account_roles")
class AccountRolesEntity(uuid: UUID? = null,
                         @Column(nullable = false) val accountUuid: UUID,
                         @OneToOne val role: RoleEntity) : BaseEntity(uuid)