package nl.benhadi.lingogamebepv4.account.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import nl.benhadi.lingogamebepv4.game.entity.GameEntity
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "account")
class AccountEntity(uuid: UUID? = null,
                    @Column(nullable = false) val email: String,
                    @Column(nullable = false) val password: String,
                    @OneToMany(
                            mappedBy = "accountUuid",
                            cascade = [CascadeType.ALL],
                            fetch = FetchType.EAGER,
                            orphanRemoval = true
                    ) val games: List<GameEntity>,
                    @OneToMany(
                            mappedBy = "accountUuid",
                            cascade = [CascadeType.ALL],
                            fetch = FetchType.LAZY,
                            orphanRemoval = true
                    ) val accountRolesEntity: List<AccountRolesEntity>
) : BaseEntity(uuid)