package nl.benhadi.lingogamebepv4.account

import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import java.util.UUID

data class AccountAuthentication(val account: AccountDTO) : Authentication {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return account.authorities
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {
        throw UnsupportedOperationException()
    }

    override fun getName(): String {
        return account.username
    }

    override fun getCredentials(): Any {
        return account.password
    }

    override fun getPrincipal(): Any {
        return account
    }

    override fun isAuthenticated(): Boolean {
        return true
    }

    override fun getDetails(): Any {
        return account
    }

    fun getAccountUuid(): UUID {
        return account.uuid
    }
}