package nl.benhadi.lingogamebepv4.account.dto

import nl.benhadi.lingogamebepv4.role.dto.RoleDTO
import java.util.UUID

data class AccountRolesDTO (val uuid: UUID, val accountUuid: UUID, val role: RoleDTO)