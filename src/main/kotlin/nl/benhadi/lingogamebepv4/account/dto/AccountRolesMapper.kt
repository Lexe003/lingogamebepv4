package nl.benhadi.lingogamebepv4.account.dto

import nl.benhadi.lingogamebepv4.account.entity.AccountRolesEntity
import nl.benhadi.lingogamebepv4.role.dto.RoleDTO
import nl.benhadi.lingogamebepv4.role.dto.RoleMapper
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class AccountRolesMapper(private val roleMapper: RoleMapper) {

    fun map(accountRolesDTO: AccountRolesDTO): AccountRolesEntity {
        return AccountRolesEntity(
                uuid = accountRolesDTO.uuid,
                accountUuid = accountRolesDTO.accountUuid,
                role = roleMapper.map(accountRolesDTO.role)
        )
    }

    fun map(accountRolesEntity: AccountRolesEntity): AccountRolesDTO {
        return AccountRolesDTO(
                uuid = accountRolesEntity.id,
                accountUuid = accountRolesEntity.accountUuid,
                role = roleMapper.map(accountRolesEntity.role)
        )
    }

    fun map(accountUuid: UUID, role: RoleDTO) : AccountRolesEntity {
        return AccountRolesEntity(
                accountUuid = accountUuid,
                role = roleMapper.map(role)
        )
    }
}