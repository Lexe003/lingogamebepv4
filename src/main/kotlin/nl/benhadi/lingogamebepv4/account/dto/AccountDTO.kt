package nl.benhadi.lingogamebepv4.account.dto

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.game.entity.GameEntity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

data class AccountDTO(
        val uuid: UUID,
        val email: String,
        private val password: String,
        val accountRoles: List<AccountRolesDTO>,
        val games: List<GameDTO>
) : UserDetails {

    @JsonIgnore
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return accountRoles.map {
            SimpleGrantedAuthority(it.role.name)
        }.toMutableSet()
    }

    @JsonIgnore
    override fun isEnabled(): Boolean {
        return true
    }

    @JsonIgnore
    override fun getUsername(): String {
        return email
    }

    @JsonIgnore
    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    @JsonIgnore
    override fun getPassword(): String {
        return password
    }

    @JsonIgnore
    override fun isAccountNonExpired(): Boolean {
        return true
    }

    @JsonIgnore
    override fun isAccountNonLocked(): Boolean {
        return true
    }
}