package nl.benhadi.lingogamebepv4.account.dto

import nl.benhadi.lingogamebepv4.account.entity.AccountEntity
import nl.benhadi.lingogamebepv4.game.dto.GameMapper
import nl.benhadi.lingogamebepv4.game.entity.GameEntity
import org.springframework.stereotype.Component

@Component
class AccountMapper(private val accountRolesMapper: AccountRolesMapper,
                    private val gameMapper: GameMapper) {

    fun map(account: AccountEntity): AccountDTO {
        return AccountDTO(
                uuid = account.id,
                email = account.email,
                password = account.password,
                accountRoles = account.accountRolesEntity.map {
                    accountRolesMapper.map(it)
                },
                games = account.games.map { gameMapper.map(it) }
        )
    }

    fun map(account: AccountDTO) : AccountEntity {
        return AccountEntity(
                uuid = account.uuid,
                email = account.email,
                password = account.password,
                accountRolesEntity = account.accountRoles.map {
                    accountRolesMapper.map(it)
                },
                games = account.games.map { gameMapper.map(it) }
        )
    }

    fun map(email: String, password: String, roles: List<AccountRolesDTO>, gameEntities: List<GameEntity>) : AccountEntity {
        return AccountEntity(
                email = email,
                password = password,
                accountRolesEntity = roles.map {
                    accountRolesMapper.map(it)
                },
                games = gameEntities
        )
    }
}