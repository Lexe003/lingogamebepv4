package nl.benhadi.lingogamebepv4.account.request

class RegisterAccountRequest(val username: String,
                             val password: String)