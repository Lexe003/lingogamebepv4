package nl.benhadi.lingogamebepv4

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

val roles = mutableListOf<String>()
const val PLAYER = "PLAYER"
const val TOTAL_GAME_ROUNDS = 6

@SpringBootApplication
class Lingogamebepv4Application

fun main(args: Array<String>) {
    runApplication<Lingogamebepv4Application>(*args)
}
