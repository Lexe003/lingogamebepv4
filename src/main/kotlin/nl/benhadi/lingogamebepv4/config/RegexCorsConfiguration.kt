package nl.benhadi.lingogamebepv4.config

import org.springframework.web.cors.CorsConfiguration
import java.util.regex.Pattern

class RegexCorsConfiguration : CorsConfiguration {

    private val allowedOriginPatterns: List<Pattern>

    constructor(allowedOrigins: List<Pattern>) {
        this.allowedOriginPatterns = allowedOrigins
    }

    constructor(other: CorsConfiguration, allowedOrigins: List<Pattern>) : this(allowedOrigins = allowedOrigins) {
        super.setAllowedOrigins(other.allowedOrigins)
        super.setAllowedMethods(other.allowedMethods)
        super.setAllowedHeaders(other.allowedHeaders)
        super.setExposedHeaders(other.exposedHeaders)
        super.setAllowCredentials(other.allowCredentials)
        super.setMaxAge(other.maxAge)
    }

    override fun combine(other: CorsConfiguration?): CorsConfiguration? {
        val combine = super.combine(other) ?: return this
        return RegexCorsConfiguration(combine, allowedOriginPatterns)
    }

    override fun checkOrigin(requestOrigin: String?): String? {
        val result = super.checkOrigin(requestOrigin)
        return result ?: checkOriginWithRegularExpression(requestOrigin)
    }

    private fun checkOriginWithRegularExpression(requestOrigin: String?): String? {
        if(requestOrigin == null) {
            return null
        }
        return allowedOriginPatterns.stream()
                .filter { pattern -> pattern.matcher(requestOrigin).matches() }
                .map { requestOrigin }
                .findFirst()
                .orElse(null)
    }

}