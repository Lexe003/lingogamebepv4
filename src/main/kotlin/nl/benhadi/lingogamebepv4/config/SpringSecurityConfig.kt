package nl.benhadi.lingogamebepv4.config

import com.fasterxml.jackson.databind.ObjectMapper
import nl.benhadi.lingogamebepv4.account.service.AccountRolesService
import nl.benhadi.lingogamebepv4.config.jwt.*
import nl.benhadi.lingogamebepv4.config.jwt.service.JwtCookieService
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SpringSecurityConfig(val accountService: UserDetailsService,
                           val objectMapper: ObjectMapper,
                           val jwtProperties: JwtProperties,
                           val jwtCookieService: JwtCookieService,
                           val accountRolesService: AccountRolesService) : WebSecurityConfigurerAdapter() {

    private val WHITELIST = arrayOf(
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/preview/**",
            "/preprocess/**",
            "/accountActivation",
            "/activationLink",
            "/account/register",
            SecurityConstants.TOKEN_URL)

    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers(*WHITELIST).permitAll()
                .requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(JwtAuthenticationFilter(authenticationManager(), objectMapper, jwtProperties, jwtCookieService))
                .addFilter(JwtAuthorizationFilter(authenticationManager(), jwtProperties, accountRolesService))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(accountService)
                .passwordEncoder(bCryptPasswordEncoder())
    }

    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }
}