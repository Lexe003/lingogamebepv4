package nl.benhadi.lingogamebepv4.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import java.util.regex.Pattern

@Configuration
class CORSConfiguration(
        @Value("\${cors.origins:}#{T(java.util.Collections).emptyList()}") private val origins: List<String>,
        @Value("\${cors.originPatterns:}#{T(java.util.Collections).emptyList()}") private val originPatterns: List<String>) {

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = RegexCorsConfiguration(originPatterns.map { Pattern.compile(it) }.toList())

        configuration.allowedOrigins = origins
        configuration.allowCredentials = true
        configuration.allowedMethods = listOf("*")
        configuration.allowedHeaders = listOf("*")
        configuration.maxAge = 1800L

        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }
}