package nl.benhadi.lingogamebepv4.config.jwt.service

import com.google.common.net.InternetDomainName
import nl.benhadi.lingogamebepv4.config.jwt.JwtProperties
import nl.benhadi.lingogamebepv4.config.jwt.SecurityConstants
import nl.benhadi.lingogamebepv4.roles
import org.springframework.stereotype.Service
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest

@Service
class JwtCookieService(private val jwtProperties: JwtProperties) {

    fun createRemoveCookie(request: HttpServletRequest): Cookie {
        roles.clear()
        return Cookie(SecurityConstants.COOKIE_NAME, "").apply {
            this.isHttpOnly = true
            this.secure = jwtProperties.token.secureCookie
            this.domain = getCookieDomain(request)
            this.path = "/"
            this.maxAge = 0
        }
    }

    fun createCookie(request: HttpServletRequest, token: String): Cookie {
        return Cookie(SecurityConstants.COOKIE_NAME, token).apply {
            this.isHttpOnly = true
            this.secure = jwtProperties.token.secureCookie
            this.domain = getCookieDomain(request)
            this.path = "/"
            this.maxAge = jwtProperties.token.expiration.seconds.toInt()
        }
    }

    companion object {

        private fun getCookieDomain(httpServletRequest: HttpServletRequest): String {
            val serverName = httpServletRequest.serverName
            val isDomain = InternetDomainName.isValid(serverName) && InternetDomainName.from(serverName).isTopPrivateDomain
            if (isDomain) {
                return InternetDomainName.from(serverName).topPrivateDomain().toString()
            }
            return serverName
        }
    }
}

