package nl.benhadi.lingogamebepv4.config.jwt

import com.fasterxml.jackson.databind.ObjectMapper
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.IOException
import io.jsonwebtoken.security.Keys.hmacShaKeyFor
import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.config.jwt.service.JwtCookieService
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import java.time.Instant
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JwtAuthenticationFilter(private val authManager: AuthenticationManager,
                              private val objectMapper: ObjectMapper,
                              private val jwtProperties: JwtProperties,
                              private val jwtCookieService: JwtCookieService) : UsernamePasswordAuthenticationFilter() {

    init {
        this.setRequiresAuthenticationRequestMatcher(AntPathRequestMatcher(SecurityConstants.TOKEN_URL, "POST"))
    }

    @Throws(AuthenticationException::class, IOException::class, ServletException::class)
    override fun attemptAuthentication(req: HttpServletRequest, res: HttpServletResponse): Authentication {

        val user = objectMapper.readValue(req.inputStream, TokenRequest::class.java)

        return authManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        user.email,
                        user.password
                )
        )
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse,
                                          filterChain: FilterChain?, authentication: Authentication) {

        val user = authentication.principal as AccountDTO

        val signingKey = jwtProperties.token.secret.toByteArray()
        val roles = user.accountRoles.map {
            it.role.name
        }
        val token = Jwts.builder()
                .signWith(hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
                .setHeaderParam("typ", SecurityConstants.TOKEN_TYPE)
                .setIssuer(jwtProperties.token.issuer)
                .setAudience(jwtProperties.token.audience)
                .setSubject(user.username)
                .setExpiration(Date.from(Instant.now().plus(jwtProperties.token.expiration)))
                .setId(user.uuid.toString())
                .claim(SecurityConstants.TOKEN_CLAIM_ROLE, roles)
                .compact()

        response.addCookie(jwtCookieService.createCookie(request, token))
    }

    data class TokenRequest(val email: String, val password: String)
}
