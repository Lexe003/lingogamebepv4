package nl.benhadi.lingogamebepv4.config.jwt

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import java.time.Duration
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Component
@ConfigurationProperties(prefix = "jwt")
class JwtProperties {

    var token = Token()

    class Token {

        @field:[NotNull]
        lateinit var expiration: Duration
        @field:[NotNull NotEmpty]
        lateinit var issuer: String
        @field:[NotNull NotEmpty]
        lateinit var audience: String
        @field:[NotNull NotEmpty]
        lateinit var secret: String
        var secureCookie: Boolean = true
    }
}