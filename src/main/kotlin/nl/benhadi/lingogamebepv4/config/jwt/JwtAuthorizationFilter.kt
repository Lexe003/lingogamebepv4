package nl.benhadi.lingogamebepv4.config.jwt

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.UnsupportedJwtException
import io.jsonwebtoken.io.IOException
import io.jsonwebtoken.security.SignatureException
import nl.benhadi.lingogamebepv4.account.AccountAuthentication
import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.account.service.AccountRolesService
import nl.benhadi.lingogamebepv4.roles
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(authManager: AuthenticationManager,
                             private val jwtProperties: JwtProperties,
                             private val accountRolesService: AccountRolesService) : BasicAuthenticationFilter(authManager) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val authentication = getAuthentication(request)

        if (authentication == null) {
            filterChain.doFilter(request, response)
            return
        }

        authentication.account.accountRoles.map {
            roles.add(it.role.name)
        }

        SecurityContextHolder.getContext().authentication = authentication
        filterChain.doFilter(request, response)
    }

    private fun getAuthentication(request: HttpServletRequest): AccountAuthentication? {
        val token = request.cookies?.asSequence()
                ?.filter { it.name == SecurityConstants.COOKIE_NAME }
                ?.filter { it.value.isNotEmpty() }
                ?.map { it.value }
                ?.firstOrNull()

        if (token != null) {
            try {
                val signingKey = jwtProperties.token.secret.toByteArray()

                val parsedToken = Jwts.parser()
                        .setSigningKey(signingKey)
                        .parseClaimsJws(token)

                val username: String = parsedToken
                        .body
                        .subject

                val uuid: UUID = UUID.fromString(parsedToken
                        .body
                        .id)

                val accountRoles = (parsedToken.body[SecurityConstants.TOKEN_CLAIM_ROLE] as List<*>)
                        .mapNotNull {
                            accountRolesService.findAccountRolesByUserUuidAndRoleName(uuid, it as String)
                        }

                return AccountAuthentication(AccountDTO(uuid, username, "", accountRoles, emptyList()))
            } catch (exception: ExpiredJwtException) {
                log.warn("Request to parse expired JWT : {} failed : {}", token, exception.message)
            } catch (exception: UnsupportedJwtException) {
                log.warn("Request to parse unsupported JWT : {} failed : {}", token, exception.message)
            } catch (exception: MalformedJwtException) {
                log.warn("Request to parse invalid JWT : {} failed : {}", token, exception.message)
            } catch (exception: SignatureException) {
                log.warn("Request to parse JWT with invalid signature : {} failed : {}", token, exception.message)
            } catch (exception: IllegalArgumentException) {
                log.warn("Request to parse empty or null JWT : {} failed : {}", token, exception.message)
            }

        }

        return null
    }

}