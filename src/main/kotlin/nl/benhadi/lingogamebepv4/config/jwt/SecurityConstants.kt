package nl.benhadi.lingogamebepv4.config.jwt

object SecurityConstants {
    const val TOKEN_URL = "/token"
    const val TOKEN_TYPE = "JWT"
    const val TOKEN_CLAIM_ROLE = "role"
    const val COOKIE_NAME = "lingo-game-token"
}