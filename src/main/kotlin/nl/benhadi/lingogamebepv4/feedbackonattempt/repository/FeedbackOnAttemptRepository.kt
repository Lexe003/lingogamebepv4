package nl.benhadi.lingogamebepv4.feedbackonattempt.repository

import nl.benhadi.lingogamebepv4.feedbackonattempt.entity.FeedbackOnAttemptEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface FeedbackOnAttemptRepository: JpaRepository<FeedbackOnAttemptEntity, UUID>