package nl.benhadi.lingogamebepv4.feedbackonattempt.entity

import nl.benhadi.lingogamebepv4.entity.BaseEntity
import nl.benhadi.lingogamebepv4.feedback.enum.Status
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "feedback_on_attempt")
class FeedbackOnAttemptEntity(uuid: UUID? = null,
                              @Column(nullable = false) val feedbackUuid: UUID,
                              @Column(nullable = false) val character: Char,
                              @Column(nullable = false) @Enumerated(EnumType.STRING) val status: Status
) : BaseEntity(uuid)