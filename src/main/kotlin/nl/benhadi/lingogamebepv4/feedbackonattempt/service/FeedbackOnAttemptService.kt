package nl.benhadi.lingogamebepv4.feedbackonattempt.service

import nl.benhadi.lingogamebepv4.feedback.enum.Status
import nl.benhadi.lingogamebepv4.feedbackonattempt.dto.FeedbackOnAttemptDTO
import nl.benhadi.lingogamebepv4.feedbackonattempt.dto.FeedbackOnAttemptMapper
import nl.benhadi.lingogamebepv4.feedbackonattempt.entity.FeedbackOnAttemptEntity
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*

@Service
class FeedbackOnAttemptService(private val feedbackOnAttemptMapper: FeedbackOnAttemptMapper,
                                private val wordService: WordService) {


    fun processAttemptByStatus(feedbackUUID: UUID, guessedWord: String, status: Status) : List<FeedbackOnAttemptEntity> {
        return guessedWord.map {
            FeedbackOnAttemptEntity(feedbackUuid = feedbackUUID, character = it, status = status)
        }
    }

    fun validate(feedbackUUID: UUID, wordToGuess: String, guessedWord: String, timeLastAttempt: Instant): List<FeedbackOnAttemptEntity>? {
        val hasAnsweredWithinTimeLimits = Instant.now().epochSecond - timeLastAttempt.epochSecond <= 10
        val wordStartsWithGivenChar: Boolean by lazy { wordToGuess.first() == guessedWord.first() }
        val wordExists: Boolean by lazy { wordService.wordExists(guessedWord) }
        val guessedWordHasValidLength: Boolean by lazy { wordToGuess.length == guessedWord.length }
        val wordHasCapitalizedChar: Boolean by lazy { guessedWord.any { it.isUpperCase() } }
        val wordHasNoSpecialChars: Boolean by lazy { guessedWord.matches(Regex("^[a-z]{5,7}$")) }
        val wordGuessed: Boolean by lazy { wordToGuess == guessedWord }

        if (!hasAnsweredWithinTimeLimits || !wordStartsWithGivenChar || !wordExists || !guessedWordHasValidLength || wordHasCapitalizedChar || !wordHasNoSpecialChars) {
            return processAttemptByStatus(feedbackUUID, guessedWord, Status.INVALID)
        }

        when (wordGuessed) {
            true -> return processAttemptByStatus(feedbackUUID, guessedWord, Status.CORRECT)
        }

        return null
    }

    fun process(feedbackUUID: UUID, wordToGuess: String, guessedWord: String, timeLastAttempt: Instant): List<FeedbackOnAttemptEntity> {

        validate(feedbackUUID, wordToGuess, guessedWord, timeLastAttempt)?.let {
            return it
        }

        val wordToGuessCharArray: CharArray = wordToGuess.toCharArray()
        val guessWordCharArray: CharArray = guessedWord.toCharArray()
        val feedbackOnAttempts = mutableListOf<FeedbackOnAttemptDTO>()


        for (l in guessWordCharArray) {
            val feedbackOnCharacter = FeedbackOnAttemptDTO(UUID.randomUUID(), feedbackUUID, l, Status.ABSENT)
            feedbackOnAttempts.add(feedbackOnCharacter)
        }

        for ((i, feedbackOnCharacter) in feedbackOnAttempts.withIndex()) {
            if (wordToGuessCharArray[i] == feedbackOnCharacter.character) {
                feedbackOnCharacter.status = Status.CORRECT
                wordToGuessCharArray[i] = 0.toChar()
            }
        }

        for ((i, c) in wordToGuessCharArray.withIndex()) {
            for (feedbackOnCharacter in feedbackOnAttempts) {
                if (feedbackOnCharacter.character == c && feedbackOnCharacter.status !== Status.CORRECT) {
                    feedbackOnCharacter.status = Status.PRESENT
                    wordToGuessCharArray[i] = 0.toChar()
                }
            }
        }

        return feedbackOnAttempts.map { feedbackOnAttemptMapper.map(it) }
    }
}