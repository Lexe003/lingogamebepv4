package nl.benhadi.lingogamebepv4.feedbackonattempt.dto

import nl.benhadi.lingogamebepv4.feedback.enum.Status
import nl.benhadi.lingogamebepv4.feedbackonattempt.entity.FeedbackOnAttemptEntity
import org.springframework.stereotype.Component
import java.util.*

@Component
class FeedbackOnAttemptMapper {

    fun map(feedbackOnAttemptDTO: FeedbackOnAttemptDTO) : FeedbackOnAttemptEntity {
        return FeedbackOnAttemptEntity(
                uuid = feedbackOnAttemptDTO.uuid,
                feedbackUuid = feedbackOnAttemptDTO.feedbackUuid,
                character = feedbackOnAttemptDTO.character,
                status = feedbackOnAttemptDTO.status
        )
    }

    fun map(feedbackOnAttemptEntity: FeedbackOnAttemptEntity) : FeedbackOnAttemptDTO {
        return FeedbackOnAttemptDTO(
                uuid = feedbackOnAttemptEntity.id,
                feedbackUuid = feedbackOnAttemptEntity.feedbackUuid,
                character = feedbackOnAttemptEntity.character,
                status = feedbackOnAttemptEntity.status

        )
    }

    fun map(feedbackUuid: UUID, char: Char, status: Status) : FeedbackOnAttemptEntity {
        return FeedbackOnAttemptEntity(
                feedbackUuid= feedbackUuid,
                status = status,
                character = char
        )
    }

}