package nl.benhadi.lingogamebepv4.feedbackonattempt.dto

import nl.benhadi.lingogamebepv4.feedback.enum.Status
import java.util.*

data class FeedbackOnAttemptDTO(var uuid: UUID,
                                val feedbackUuid: UUID,
                                var character: Char,
                                var status: Status)