package nl.benhadi.lingogamebepv4.feedback.repository

import nl.benhadi.lingogamebepv4.feedback.entity.FeedbackEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface FeedbackRepository: JpaRepository<FeedbackEntity, UUID>