package nl.benhadi.lingogamebepv4.feedback.enum

enum class Status {
    CORRECT, ABSENT, INVALID, PRESENT
}