package nl.benhadi.lingogamebepv4.feedback.entity

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import nl.benhadi.lingogamebepv4.entity.BaseEntity
import nl.benhadi.lingogamebepv4.feedbackonattempt.entity.FeedbackOnAttemptEntity
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "feedback")
class FeedbackEntity(uuid: UUID? = null,
                     @Column(nullable = false) val gameRoundUuid: UUID,
                     @Column(nullable = false) val guessedWord: String,
                     @OneToMany(
                             mappedBy = "feedbackUuid",
                             cascade = [CascadeType.ALL],
                             fetch = FetchType.EAGER,
                             orphanRemoval = true
                     ) var feedbackOnAttempts: List<FeedbackOnAttemptEntity>,
                     val creationTime: Instant = Instant.now()
) : BaseEntity(uuid)