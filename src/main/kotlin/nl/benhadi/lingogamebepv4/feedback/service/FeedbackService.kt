package nl.benhadi.lingogamebepv4.feedback.service

import nl.benhadi.lingogamebepv4.feedback.dto.FeedbackDTO
import nl.benhadi.lingogamebepv4.feedback.dto.FeedbackMapper
import nl.benhadi.lingogamebepv4.feedbackonattempt.service.FeedbackOnAttemptService
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.*

@Service
class FeedbackService(private val feedbackMapper: FeedbackMapper,
                      private val feedbackOnAttemptService: FeedbackOnAttemptService) {

    fun process(gameRoundUuid: UUID, guessedWord: String, wordToGuess: String, timeLastAttempt: Instant): FeedbackDTO {

        val newFeedbackObject = feedbackMapper.map(
                guessedWord = guessedWord,
                gameRoundUuid = gameRoundUuid,
                feedbackOnAttempts = emptyList()
        )

        val newAttempts = feedbackOnAttemptService.process(
                feedbackUUID = newFeedbackObject.id,
                wordToGuess = wordToGuess,
                guessedWord = guessedWord,
                timeLastAttempt = timeLastAttempt
        )

        newFeedbackObject.feedbackOnAttempts = newAttempts
        return feedbackMapper.map(newFeedbackObject)
    }
}