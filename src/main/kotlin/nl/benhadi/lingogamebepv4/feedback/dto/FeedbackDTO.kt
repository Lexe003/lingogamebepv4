package nl.benhadi.lingogamebepv4.feedback.dto

import java.util.*
import nl.benhadi.lingogamebepv4.feedbackonattempt.dto.FeedbackOnAttemptDTO
import java.time.Instant

data class FeedbackDTO(var uuid: UUID,
                       val gameRoundUuid: UUID,
                       var feedbackOnAttempts: List<FeedbackOnAttemptDTO>,
                       val guessedWord: String,
                       val creationTime: Instant)