package nl.benhadi.lingogamebepv4.feedback.dto

import nl.benhadi.lingogamebepv4.feedback.entity.FeedbackEntity
import nl.benhadi.lingogamebepv4.feedback.enum.Status
import nl.benhadi.lingogamebepv4.feedbackonattempt.dto.FeedbackOnAttemptMapper
import nl.benhadi.lingogamebepv4.feedbackonattempt.entity.FeedbackOnAttemptEntity
import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import org.springframework.stereotype.Component
import java.util.*

@Component
class FeedbackMapper(private val feedbackOnAttemptMapper: FeedbackOnAttemptMapper) {

    fun map(feedbackEntity: FeedbackEntity) : FeedbackDTO {
        return FeedbackDTO(
                uuid = feedbackEntity.id,
                gameRoundUuid = feedbackEntity.gameRoundUuid,
                feedbackOnAttempts = feedbackEntity.feedbackOnAttempts.map { feedbackOnAttemptMapper.map(it) },
                guessedWord = feedbackEntity.guessedWord,
                creationTime = feedbackEntity.creationTime
        )
    }

    fun map(feedbackDTO: FeedbackDTO): FeedbackEntity {
        return FeedbackEntity(
                uuid = feedbackDTO.uuid,
                gameRoundUuid = feedbackDTO.gameRoundUuid,
                feedbackOnAttempts = feedbackDTO.feedbackOnAttempts.map { feedbackOnAttemptMapper.map(it) },
                guessedWord = feedbackDTO.guessedWord,
                creationTime = feedbackDTO.creationTime
        )
    }

    fun map(gameRoundUuid: UUID, guessedWord: String, feedbackOnAttempts: List<FeedbackOnAttemptEntity>): FeedbackEntity {
        return FeedbackEntity(
                gameRoundUuid = gameRoundUuid,
                guessedWord = guessedWord,
                feedbackOnAttempts = feedbackOnAttempts
        )
    }

    fun mapInit(gameRoundUuid: UUID, wordToGuess: WordEntity) : FeedbackEntity {
        val feedbackUuid = UUID.randomUUID()
        return FeedbackEntity(
                uuid = feedbackUuid,
                gameRoundUuid = gameRoundUuid,
                feedbackOnAttempts = mutableListOf(
                                                feedbackOnAttemptMapper.map(
                                                        feedbackUuid = feedbackUuid,
                                                        char = wordToGuess.name[0],
                                                        status = Status.CORRECT)),
                guessedWord = wordToGuess.name.first().toString()
        )
    }

}