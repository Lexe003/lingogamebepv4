CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE account
(
    uuid uuid NOT NULL primary key,
    email varchar(255) not null constraint account_email_key unique,
    password varchar(60) not null
);

create table role
(
    uuid uuid not null primary key,
    name varchar(255) not null constraint role_name_key unique
);

create table account_roles
(
    uuid uuid not null primary key,
    account_uuid uuid not null constraint account_roles_account_uuid_fkey references account ON DELETE CASCADE,
    role_uuid uuid not null constraint account_roles_role_uuid_fkey references role ON DELETE CASCADE,
    constraint account_roles_account_uuid_role_uuid_key unique (account_uuid, role_uuid)
);

CREATE TABLE game
(
    uuid UUID NOT NULL PRIMARY KEY,
    account_uuid UUID NOT NULL CONSTRAINT game_account_uuid_fkey references account (uuid) ON DELETE CASCADE
);

CREATE TABLE score
(
    uuid UUID NOT NULL PRIMARY KEY,
    amount INT,
    name CHARACTER VARYING(255) NOT NULL,
    game_uuid UUID NOT NULL CONSTRAINT score_game_uuid_fkey references game (uuid) ON DELETE CASCADE
);

-- ALTER TABLE game
--     ADD CONSTRAINT game_score_uuid_fkey FOREIGN KEY (score_uuid) REFERENCES score (uuid);

CREATE TABLE word
(
    uuid UUID NOT NULL PRIMARY KEY,
    name CHARACTER VARYING(255) NOT NULL UNIQUE
);

CREATE TABLE game_round
(
    uuid UUID NOT NULL PRIMARY KEY,
    word_to_guess_uuid UUID NOT NULL CONSTRAINT game_round_word_fkey references word (uuid) ON DELETE CASCADE,
    game_uuid UUID NOT NULL CONSTRAINT game_round_game_fkey references game (uuid)  ON DELETE CASCADE,
    can_attempt BOOLEAN NOT NULL,
    creation_time TIMESTAMP WITH time zone DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE feedback
(
    uuid UUID NOT NULL PRIMARY KEY,
    game_round_uuid UUID NOT NULL CONSTRAINT feedback_game_round_uuid_fkey references game_round ON DELETE CASCADE,
    guessed_word CHARACTER VARYING(255) NOT NULL
);

CREATE TABLE feedback_on_attempt
(
    uuid UUID NOT NULL PRIMARY KEY,
    feedback_uuid UUID NOT NULL CONSTRAINT feedback_on_attempt_feedback_fkey references feedback ON DELETE CASCADE,
    character CHAR NOT NULL,
    status CHARACTER VARYING(255) NOT NULL
);

INSERT INTO role VALUES(uuid_generate_v4(), 'PLAYER');
INSERT INTO role VALUES(uuid_generate_v4(), 'ADMINISTRATOR');