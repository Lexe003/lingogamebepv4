package nl.benhadi.lingogamebepv4.gameround.service

import nl.benhadi.lingogamebepv4.feedback.dto.FeedbackDTO
import nl.benhadi.lingogamebepv4.feedback.service.FeedbackService
import nl.benhadi.lingogamebepv4.game.enum.Phase
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundDTO
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundMapper
import nl.benhadi.lingogamebepv4.word.dto.WordDTO
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.time.OffsetDateTime
import java.util.*

@RunWith(MockitoJUnitRunner::class)
internal class GameRoundServiceTest {

    @Mock
    private lateinit var mockFeedbackService: FeedbackService

    @Mock
    private lateinit var mockGameRoundMapper: GameRoundMapper

    @Mock
    private lateinit var mockWordService: WordService

    private lateinit var gameRoundService: GameRoundService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        gameRoundService = GameRoundService(
                feedbackService = mockFeedbackService,
                gameRoundMapper = mockGameRoundMapper,
                wordService = mockWordService
        )
    }

    @Test
    fun `game round with empty attempts should throw response status exception`() {
        val gameRoundDTO = GameRoundDTO(
                uuid = UUID.randomUUID(),
                wordToGuess = WordDTO(uuid = UUID.randomUUID(), name = "woord"),
                gameUuid = UUID.randomUUID(),
                canAttempt = false,
                creationTime = OffsetDateTime.now(),
                attempts = emptyList()
        )

        assertThrows<ResponseStatusException> {
            gameRoundService.process(emptyList(), gameRoundDTO, "woord")
        }
    }

    @Test
    fun `throw response exception when no word found for new game round`() {
        val feedbackDTO = mock(FeedbackDTO::class.java)
        `when`(mockWordService.getRandomWordByPhase(Phase.ONE)).thenThrow(ResponseStatusException(HttpStatus.NOT_FOUND))
        val gameRoundDTO = GameRoundDTO(
                uuid = UUID.randomUUID(),
                wordToGuess = WordDTO(uuid = UUID.randomUUID(), name = "woord"),
                gameUuid = UUID.randomUUID(),
                canAttempt = false,
                creationTime = OffsetDateTime.now(),
                attempts = listOf(feedbackDTO)
        )

        assertThrows<ResponseStatusException> {
            gameRoundService.process(emptyList(), gameRoundDTO, "woord")
        }
    }
}