package nl.benhadi.lingogamebepv4.architecture

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.lang.ArchRule
import org.junit.jupiter.api.Test
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes

class LayerTest {

    private val importedClasses: JavaClasses = ClassFileImporter().importPackages("nl.benhadi.lingogamebepv4")

    @Test
    fun `Repositories should only be accessed by services`() {
        val serviceToRepositoryRule: ArchRule = classes()
                .that().resideInAPackage("..repository..")
                .should().onlyBeAccessed().byAnyPackage("..service..")
        serviceToRepositoryRule.check(importedClasses)
    }

    @Test
    fun `Services should only be accessed by controllers, configs and services`() {
        val controllerToServiceRule: ArchRule = classes()
                .that().resideInAPackage("..service..")
                .should().onlyBeAccessed().byAnyPackage("..controller..", "..config..", "..service..")
        controllerToServiceRule.check(importedClasses)
    }

    @Test
    fun `Mappers should only be accessed by services and mappers`() {
        val mapperRule : ArchRule = classes()
                .that().resideInAPackage("..mapper..")
                .should().onlyBeAccessed().byAnyPackage("..service..", "..mapper..")
        mapperRule.check(importedClasses)
    }

    @Test
    fun `Classes that end with Service should only be resided in Service package`() {
        val serviceRule : ArchRule = classes()
                .that().haveSimpleNameEndingWith("Service")
                .should().resideInAPackage("..service..")
        serviceRule.check(importedClasses)
    }

    @Test
    fun `Classes that end with Repository should only be resided in Repository package`() {
        val repositoryRule : ArchRule = classes()
                .that().haveSimpleNameEndingWith("Repository")
                .should().resideInAPackage("..repository..")
        repositoryRule.check(importedClasses)
    }

    @Test
    fun `Classes that end with Entity should only be resided in Entity package`() {
        val entityRule : ArchRule = classes()
                .that().haveSimpleNameEndingWith("Entity")
                .should().resideInAPackage("..entity..")
        entityRule.check(importedClasses)
    }

    @Test
    fun `Classes that end with DTO should only be resided in dto package`() {
        val dtoRule : ArchRule = classes()
                .that().haveSimpleNameEndingWith("DTO")
                .should().resideInAPackage("..dto..")
        dtoRule.check(importedClasses)
    }
}