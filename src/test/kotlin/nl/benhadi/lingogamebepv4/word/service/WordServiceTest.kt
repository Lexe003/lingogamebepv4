package nl.benhadi.lingogamebepv4.word.service

import nl.benhadi.lingogamebepv4.game.enum.Phase
import nl.benhadi.lingogamebepv4.score.dto.ScoreMapper
import nl.benhadi.lingogamebepv4.score.repository.ScoreRepository
import nl.benhadi.lingogamebepv4.score.service.ScoreService
import nl.benhadi.lingogamebepv4.word.dto.WordMapper
import nl.benhadi.lingogamebepv4.word.entity.WordEntity
import nl.benhadi.lingogamebepv4.word.repository.WordRepository
import nl.benhadi.lingogamebepv4.word.request.WordRequest
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.*

class WordServiceTest {

    private lateinit var wordService: WordService

    @Mock
    private lateinit var mockWordMapper: WordMapper

    @Mock
    private lateinit var mockWordRepository: WordRepository

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        wordService = WordService(wordMapper = mockWordMapper, wordRepository = mockWordRepository)
    }


    @Test
    fun findAllTest() {
        `when`(mockWordRepository.findAll()).thenReturn(listOf(
                WordEntity(UUID.randomUUID(), "vogel"),
                WordEntity(UUID.randomUUID(), "traan")))
        assertEquals(2, wordService.findAll().size)
    }

    @Test
    fun getRandomWordByPhaseTest() {
        val uuid = UUID.randomUUID()
        `when`(mockWordRepository.findRandomByLength(7)).thenReturn(listOf(WordEntity(uuid, "politie")))
        assertEquals(WordEntity(uuid, "politie"), wordService.getRandomWordByPhase(Phase.THREE))
    }

    @Test
    fun wordExistsTest() {
        val word = "vogel"
        `when`(mockWordRepository.existsWordEntityByName(word)).thenReturn(true)
        assertEquals(true, wordService.wordExists(word))
    }

    @Test
    fun saveAllTest() {
        val listOfWords = listOf(WordRequest("vogel"), WordRequest("kraag"))
        val listOfEntities = listOf(WordEntity(UUID.randomUUID(), "vogel"), WordEntity(UUID.randomUUID(), "kraag"))

        for (i in listOfWords.indices) {
            `when`(mockWordMapper.map(listOfWords[i])).thenReturn(listOfEntities[i])
        }

        wordService.saveAll(listOfWords)
        verify(mockWordRepository, times(1)).saveAll(listOfEntities)

        for (i in listOfWords.indices) {
            verify(mockWordMapper, times(1)).map(listOfWords[i])
        }
    }

}