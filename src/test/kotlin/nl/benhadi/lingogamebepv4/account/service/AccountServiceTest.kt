package nl.benhadi.lingogamebepv4.account.service

import junit.framework.Assert
import nl.benhadi.lingogamebepv4.account.dto.AccountMapper
import nl.benhadi.lingogamebepv4.account.dto.AccountRolesMapper
import nl.benhadi.lingogamebepv4.account.repository.AccountRepository
import nl.benhadi.lingogamebepv4.game.dto.GameMapper
import nl.benhadi.lingogamebepv4.role.service.RoleService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.security.core.userdetails.UsernameNotFoundException

internal class AccountServiceTest {

    private lateinit var accountService: AccountService

    @Mock
    private lateinit var mockAccountRepository: AccountRepository

    @Mock
    private lateinit var mockAccountMapper: AccountMapper

    @Mock
    private lateinit var mockAccountRolesService: AccountRolesService

    @Mock
    private lateinit var mockAccountRolesMapper: AccountRolesMapper

    @Mock
    private lateinit var mockRoleService: RoleService

    @Mock
    private lateinit var mockGameMapper: GameMapper


    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        accountService = AccountService(
                accountMapper = mockAccountMapper,
                accountRepository = mockAccountRepository,
                accountRolesMapper = mockAccountRolesMapper,
                accountRolesService = mockAccountRolesService,
                roleService = mockRoleService,
                gameMapper = mockGameMapper
        )
    }


    @Test
    fun `when username not found throw exception`() {
        val exception = assertThrows<UsernameNotFoundException> {
            accountService.loadUserByUsername("sample")
        }
        assertEquals(exception.message, "sample")
    }
}