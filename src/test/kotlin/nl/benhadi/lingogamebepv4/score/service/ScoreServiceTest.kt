package nl.benhadi.lingogamebepv4.score.service

import nl.benhadi.lingogamebepv4.score.dto.ScoreDTO
import nl.benhadi.lingogamebepv4.score.dto.ScoreMapper
import nl.benhadi.lingogamebepv4.score.entity.ScoreEntity
import nl.benhadi.lingogamebepv4.score.repository.ScoreRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.*
import org.mockito.Mock

internal class ScoreServiceTest {

    private lateinit var scoreService: ScoreService

    @Mock
    private lateinit var mockScoreMapper: ScoreMapper

    @Mock
    private lateinit var mockScoreRepository: ScoreRepository

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        scoreService = ScoreService(scoreRepository = mockScoreRepository, scoreMapper = mockScoreMapper)
    }

    @Test
    fun findAll() {

        val listOfScoreEntities = listOf(ScoreEntity(UUID.randomUUID(), 50, UUID.randomUUID(), "hans"),
                ScoreEntity(UUID.randomUUID(), 50, UUID.randomUUID(), "yassine"))
        `when`(mockScoreRepository.findAll()).thenReturn(listOfScoreEntities)

        listOfScoreEntities.forEach {
            `when`(mockScoreMapper.map(it)).thenReturn(
                    ScoreDTO(
                            uuid = it.id,
                            amount = it.amount,
                            gameUuid = it.gameUuid,
                            name = it.name
                    )
            )
        }

        val result = scoreService.findAll()
        verify(mockScoreRepository, times(1)).findAll()

        for (i in result.indices) {
            verify(mockScoreMapper, times(1)).map(listOfScoreEntities[i])
        }

        assertEquals(result.size, 2)
    }
}