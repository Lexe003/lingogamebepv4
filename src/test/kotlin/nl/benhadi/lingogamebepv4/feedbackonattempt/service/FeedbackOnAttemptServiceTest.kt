package nl.benhadi.lingogamebepv4.feedbackonattempt.service

import nl.benhadi.lingogamebepv4.feedback.enum.Status
import nl.benhadi.lingogamebepv4.feedbackonattempt.dto.FeedbackOnAttemptMapper
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.time.Instant
import java.util.*
import java.util.stream.Stream

@RunWith(MockitoJUnitRunner::class)
class FeedbackOnAttemptServiceTest {

    companion object {

        @JvmStatic
        private fun wordProvider(): Stream<Arguments> {
            return Stream.of(
                    Arguments.of("baard", "kassa", Instant.now(), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID)),
                    Arguments.of("baard", "baaaaaard", Instant.now(), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID)),
                    Arguments.of("baard", "bAaRd", Instant.now(), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID)),
                    Arguments.of("baard", "bAaRd", Instant.now(), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID)),
                    Arguments.of("baard", "b#13#", Instant.now(), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID)),
                    Arguments.of("baard", "barst", Instant.now(), listOf(Status.CORRECT, Status.CORRECT, Status.PRESENT, Status.ABSENT, Status.ABSENT)),
                    Arguments.of("baard", "barak", Instant.now(), listOf(Status.CORRECT, Status.CORRECT, Status.PRESENT, Status.PRESENT, Status.ABSENT)),
                    Arguments.of("baard", "baaaa", Instant.now(), listOf(Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.ABSENT, Status.ABSENT)),
                    Arguments.of("baard", "baard", Instant.now(), listOf(Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.CORRECT)),
                    Arguments.of("aaltjes", "aaaaaaa", Instant.now(), listOf(Status.CORRECT, Status.CORRECT, Status.ABSENT, Status.ABSENT, Status.ABSENT, Status.ABSENT, Status.ABSENT))
            )
        }

        @JvmStatic
        private fun timeProvider(): Stream<Arguments> {
            return Stream.of(
                    Arguments.of("baard", "baard", Instant.now().minusSeconds(9), listOf(Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.CORRECT)),
                    Arguments.of("baard", "baard", Instant.now().minusSeconds(10), listOf(Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.CORRECT, Status.CORRECT)),
                    Arguments.of("baard", "baard", Instant.now().minusSeconds(11), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID)),
                    Arguments.of("baard", "baard", Instant.now().minusSeconds(11), listOf(Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID, Status.INVALID))
            )
        }
    }

    @ParameterizedTest
    @MethodSource("wordProvider", "timeProvider")
    fun verifyFeedback(wordToGuess: String, guessedWord: String, creationTime: Instant, expected: List<Status>) {
        val wordServiceMock = mock(WordService::class.java)
        `when`(wordServiceMock.wordExists(guessedWord)).thenReturn(true)

        val feedbackOnAttemptMapper = FeedbackOnAttemptMapper()
        val feedbackOnAttemptService = FeedbackOnAttemptService(feedbackOnAttemptMapper, wordServiceMock)
        val feedbackOnAttempts = feedbackOnAttemptService.process(UUID.randomUUID(), wordToGuess, guessedWord, creationTime)
        val listWithStatus = feedbackOnAttempts.map { it.status }

        assertEquals(listWithStatus, expected)
    }
}