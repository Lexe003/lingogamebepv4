package nl.benhadi.lingogamebepv4.common

import nl.benhadi.lingogamebepv4.common.context.KafkaTestContext
import nl.benhadi.lingogamebepv4.common.context.PostgresTestContext
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.ClassPathResource
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(initializers = [PostgresTestContext::class, KafkaTestContext::class])
abstract class IntegrationTest

fun ClassPathResource.readText() : String {
    return this.inputStream.reader().readText()
}
