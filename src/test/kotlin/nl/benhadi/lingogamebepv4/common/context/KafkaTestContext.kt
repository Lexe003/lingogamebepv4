package nl.benhadi.lingogamebepv4.common.context

import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.kafka.test.EmbeddedKafkaBroker

class KafkaTestContext : ApplicationContextInitializer<ConfigurableApplicationContext> {

    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        val kafka = getKafka()
        TestPropertyValues.of(
                "spring.cloud.stream.kafka.binder.brokers=${kafka.brokersAsString}")
                .applyTo(applicationContext.environment)
    }

    companion object KafkaEmbeddedHolder {
        private val embeddedKafka = EmbeddedKafkaBroker(1, false, "lingo")

        private var started = false

        init {
            Runtime.getRuntime().addShutdownHook(Thread(Runnable(function = {
                embeddedKafka.destroy()
            })))
        }

        fun getKafka(): EmbeddedKafkaBroker {
            if (!started) {
                val brokerProperties: MutableMap<String, String> = HashMap()
                brokerProperties["log.dir"] = "out/embedded-kafka"
                embeddedKafka.brokerProperties(brokerProperties)
                embeddedKafka.afterPropertiesSet()
                started = true
            }
            return embeddedKafka
        }
    }
}
