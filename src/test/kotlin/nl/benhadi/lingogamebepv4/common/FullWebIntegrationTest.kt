package nl.benhadi.lingogamebepv4.common

import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.module.mockmvc.RestAssuredMockMvc
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.context.WebApplicationContext
import java.util.*
import javax.sql.DataSource
import nl.benhadi.lingogamebepv4.config.jwt.*
import nl.benhadi.lingogamebepv4.roles


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class FullWebIntegrationTest : IntegrationTest() {

    @LocalServerPort
    var port: Int = 0

    @Autowired
    private lateinit var webApplicationContext: WebApplicationContext

    @Autowired
    private lateinit var dataSource: DataSource

    val cookieName: String = SecurityConstants.COOKIE_NAME

    companion object {
        const val PLAYER_EMAIL = "player@hu.nl"
        const val PASSWORD = "test123"
        const val PLAYER_ROLE = "PLAYER"
    }

    @BeforeEach
    fun clearRoles() {
        roles.clear()
    }

    @BeforeEach
    fun setUp() {
        RestAssuredMockMvc.webAppContextSetup(webApplicationContext)
        RestAssured.port = port
    }

    fun getAccount(accountUUID: UUID, email: String, roles: List<String>): TestAccount {
        val selectQuery = "SELECT EXISTS(SELECT * FROM account WHERE email = ?)"
        val insertQuery = "INSERT INTO ACCOUNT VALUES (?, ?, ?)"
        val insertAccountRoleQuery = "INSERT INTO account_roles VALUES(?, ?, (SELECT UUID FROM ROLE WHERE NAME = ?))"
        val template = JdbcTemplate(dataSource)
        val accountExists = template.queryForObject(selectQuery, arrayOf(email), Boolean::class.java)

        if (!accountExists) {
            template.execute(insertQuery) { ps ->
                ps.setObject(1, accountUUID)
                ps.setString(2, email)
                ps.setString(3, BCryptPasswordEncoder().encode(PASSWORD))
                ps.execute()
            }

            roles.forEach {
                template.execute(insertAccountRoleQuery) { ps ->
                ps.setObject(1, UUID.randomUUID())
                ps.setObject(2, accountUUID)
                ps.setString(3, it)
                ps.execute()
                }
            }
        }

        val token = RestAssured.given().`when`()
                .contentType(ContentType.JSON)
                .body("{ \"email\": \"${email}\", \"password\": \"$PASSWORD\" }")
                .post(SecurityConstants.TOKEN_URL)
                .then()
                .statusCode(200)
                .extract()
                .cookie(cookieName)

        return TestAccount(token)
    }

    val playerAccount by lazy {
        val accountUUID = UUID.fromString("4876458f-b4d2-498b-bb28-935b9b950d3f")
        getAccount(accountUUID, PLAYER_EMAIL, listOf(PLAYER_ROLE))
    }

}

data class TestAccount(val token: String)