package nl.benhadi.lingogamebepv4.game.service

import org.junit.Assert.assertEquals
import nl.benhadi.lingogamebepv4.account.dto.AccountDTO
import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.game.dto.GameMapper
import nl.benhadi.lingogamebepv4.game.repository.GameRepository
import nl.benhadi.lingogamebepv4.gameround.dto.GameRoundDTO
import nl.benhadi.lingogamebepv4.gameround.service.GameRoundService
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.springframework.web.server.ResponseStatusException

internal class GameServiceTest {

    private lateinit var gameService: GameService

    @Mock
    private lateinit var mockGameDTO: GameDTO

    @Mock
    private lateinit var mockWordService: WordService

    @Mock
    private lateinit var mockGameRepository: GameRepository

    @Mock
    private lateinit var mockGameMapper: GameMapper

    @Mock
    private lateinit var mockGameRoundService: GameRoundService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        gameService = GameService(
                wordService = mockWordService,
                gameRepository = mockGameRepository,
                gameMapper = mockGameMapper,
                gameRoundService = mockGameRoundService
        )
    }

    @Test
    fun `Exception should be thrown when word service returns null value`() {
        val accountDTO = mock(AccountDTO::class.java)

        val exception = assertThrows<ResponseStatusException> {
            gameService.createNewGame(accountDTO)
        }
        assertEquals(exception.reason, "No words found in the database")
    }

    @Test
    fun `Exception should be thrown when comparator received empty array`() {
        `when`(mockGameDTO.gameRounds).thenReturn(emptyList())

        val exception = assertThrows<ResponseStatusException> {
            gameService.processAttempt(mockGameDTO, "woord")
        }
        assertEquals(exception.reason, "No game rounds found!")
    }

    @Test
    fun `Exception should be thrown when game finished`() {
        val gameRound = mock(GameRoundDTO::class.java)
        gameRound.canAttempt = false

        `when`(mockGameDTO.gameRounds).thenReturn(listOf(gameRound))

        val exception = assertThrows<ResponseStatusException> {
            gameService.processAttempt(mockGameDTO, "wooord")
        }
        assertEquals(exception.reason, "Game ended already!")
    }
}