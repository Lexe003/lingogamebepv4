package nl.benhadi.lingogamebepv4.controller

import io.restassured.RestAssured
import io.restassured.http.ContentType
import nl.benhadi.lingogamebepv4.account.request.RegisterAccountRequest
import nl.benhadi.lingogamebepv4.common.FullWebIntegrationTest
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

class AccountControllerIntegrationTest: FullWebIntegrationTest() {

    @Test
    fun `get account credentials without cookie`() {
        RestAssured.given().`when`()
                .get("account")
                .then()
                .statusCode(403)
    }

    @Test fun `get account credentials with valid tokem`() {
        RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("account")
                .then()
                .statusCode(200)
                .body("email", Matchers.equalTo("player@hu.nl"))
                .body("accountRoles.size()", Matchers.equalTo(1))
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/insertAccount.sql"]),
            Sql(scripts = ["classpath:common/cleanAccountTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test fun `register account with existing username`() {
        val registerAccountRequest = RegisterAccountRequest("email@email.com", "password")
        RestAssured.given().`when`()
                .contentType(ContentType.JSON)
                .body(registerAccountRequest)
                .post("account/register")
                .then()
                .statusCode(409)
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/cleanAccountTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `register new account with unexisting username`() {
        val registerAccountRequest = RegisterAccountRequest("mockito", "password")
        RestAssured.given().`when`()
                .contentType(ContentType.JSON)
                .body(registerAccountRequest)
                .post("account/register")
                .then()
                .statusCode(200)
                .body("email", Matchers.equalTo("mockito"))
    }

    @Test
    fun `logout test`() {
        RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .delete("token")
                .then()
                .assertThat()
                .statusCode(200)
    }
}