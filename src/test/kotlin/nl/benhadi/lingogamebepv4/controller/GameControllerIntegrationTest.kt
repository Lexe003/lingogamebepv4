package nl.benhadi.lingogamebepv4.controller

import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import nl.benhadi.lingogamebepv4.common.FullWebIntegrationTest
import nl.benhadi.lingogamebepv4.feedback.enum.Status
import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.game.request.AttemptRequest
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import java.util.*

class GameControllerIntegrationTest : FullWebIntegrationTest() {

    @SqlGroup(
            Sql(scripts = ["classpath:common/insertWords.sql"]),
            Sql(scripts = ["classpath:common/cleanWordTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `test prepare new game`() {
        given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("/game")
                .then()
                .statusCode(200)
                .body("gameRounds.size()", Matchers.equalTo(1))
                .body("gameRounds[0].wordToGuess.name", Matchers.notNullValue())
                .body("gameRounds[0].score", Matchers.nullValue())
                .body("gameRounds[0].attempts.size()", Matchers.equalTo(1))
                .body("gameRounds[0].attempts[0].guessedWord", Matchers.notNullValue())
                .body("gameRounds[0].attempts[0].feedbackOnAttempts.size()", Matchers.equalTo(1))
                .body("gameRounds[0].attempts[0].feedbackOnAttempts[0].status", Matchers.equalTo("CORRECT"))
    }

    @Test
    fun `test make new guess attempt on non existing game`() {
        given().`when`()
                .cookie(cookieName, playerAccount.token)
                .contentType(ContentType.JSON)
                .body(AttemptRequest("vogel"))
                .post("game/${UUID.randomUUID()}/attempt")
                .then()
                .statusCode(404)
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/insertWords.sql"]),
            Sql(scripts = ["classpath:common/cleanWordTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `test make first guess attempt with correct word`() {
        val response = given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("/game")

        val gameDTO: GameDTO = response.`as`(GameDTO::class.java)
        val wordToGuess = gameDTO.gameRounds[0].wordToGuess.name

        val firstAndCorrectAttemptResponse = given().`when`()
                .cookie(cookieName, playerAccount.token)
                .contentType(ContentType.JSON)
                .body(AttemptRequest("$wordToGuess"))
                .post("game/${gameDTO.uuid}/attempt")

        val gameDTOAtFirstAttempt = firstAndCorrectAttemptResponse.`as`(GameDTO::class.java)

        assertThat(gameDTOAtFirstAttempt.gameRounds.size == 2).isEqualTo(true)
        assertThat(gameDTOAtFirstAttempt.gameRounds[0].attempts.size == 2).isEqualTo(true)
        assertThat(gameDTOAtFirstAttempt.gameRounds[0].attempts[1].feedbackOnAttempts.size == 5).isEqualTo(true)

        val correctFeedbackAttempts = gameDTOAtFirstAttempt.gameRounds[0].attempts[1].feedbackOnAttempts

        for (i in correctFeedbackAttempts.indices) {
            assertThat(correctFeedbackAttempts[i].character == wordToGuess[i]).isEqualTo(true)
            assertThat(correctFeedbackAttempts[i].status == Status.CORRECT).isEqualTo(true)
        }
    }

    fun testPlayFirstRoundAndLoseAtLastAttempt(attemptRequest: AttemptRequest, gameUUID: UUID) : GameDTO {
        return given().`when`()
                .cookie(cookieName, playerAccount.token)
                .contentType(ContentType.JSON)
                .body(attemptRequest)
                .post("game/${gameUUID}/attempt")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .`as`(GameDTO::class.java)
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/insertWords.sql"]),
            Sql(scripts = ["classpath:common/cleanWordTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `test play first round and lose at last attempt`() {

        val response = given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("/game")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .`as`(GameDTO::class.java)

        val secondAttempt = testPlayFirstRoundAndLoseAtLastAttempt(AttemptRequest("viral"), response.uuid)
        validateAttempt(secondAttempt, 2, 1)

        val thirdAttempt = testPlayFirstRoundAndLoseAtLastAttempt(AttemptRequest("varol"), response.uuid)
        validateAttempt(thirdAttempt, 3, 1)

        val forthAttempt = testPlayFirstRoundAndLoseAtLastAttempt(AttemptRequest("vonat"), response.uuid)
        validateAttempt(forthAttempt, 4, 1)

        val fifthAttempt = testPlayFirstRoundAndLoseAtLastAttempt(AttemptRequest("vogel"), response.uuid)
        validateAttempt(fifthAttempt, 1, 2)
    }

    private fun validateAttempt(attempt: GameDTO, expectedFeedbackSize: Int, expectedGameRoundsSize: Int) {
        val actualGameRoundSize = attempt.gameRounds.size

        // Test amount of rounds
        assertThat(attempt.gameRounds.size).isEqualTo(expectedGameRoundsSize)

        // Test amount of feedbacks expected for current round
        assertThat(attempt.gameRounds[expectedGameRoundsSize-1].attempts.size).isEqualTo(expectedFeedbackSize)

        // Test current and previous rounds on canAttempt
        for ((index, gameRound) in attempt.gameRounds.withIndex())
        {
            if (index == actualGameRoundSize -1) {
                assertThat(gameRound.canAttempt).isEqualTo(true) // Current round
            } else {
                assertThat(gameRound.canAttempt).isEqualTo(false) // Previous rounds
            }
        }
    }

}