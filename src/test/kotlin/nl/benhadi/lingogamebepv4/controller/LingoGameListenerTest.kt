package nl.benhadi.lingogamebepv4.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.assertj.core.api.Assertions.assertThat
import nl.benhadi.lingogamebepv4.common.IntegrationTest
import nl.benhadi.lingogamebepv4.common.context.KafkaTestContext
import nl.benhadi.lingogamebepv4.common.readText
import nl.benhadi.lingogamebepv4.word.dto.WordDTO
import nl.benhadi.lingogamebepv4.word.request.WordRequest
import nl.benhadi.lingogamebepv4.word.service.WordService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.kafka.config.KafkaListenerEndpointRegistry
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.test.utils.ContainerTestUtils
import org.springframework.kafka.test.utils.KafkaTestUtils
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

class LingoGameListenerTest: IntegrationTest() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var wordService: WordService

    @Autowired
    private lateinit var kafkaListenerEndpointRegistry: KafkaListenerEndpointRegistry

    private lateinit var template: KafkaTemplate<String, String>

    @BeforeEach
    fun setUp() {
        val senderProperties = KafkaTestUtils.senderProps(KafkaTestContext.getKafka().brokersAsString)
        val producerFactory = DefaultKafkaProducerFactory<String, String>(senderProperties)
        template = KafkaTemplate(producerFactory)

        for (messageListenerContainer in kafkaListenerEndpointRegistry.listenerContainers) {
            ContainerTestUtils.waitForAssignment(messageListenerContainer,
                    KafkaTestContext.getKafka().partitionsPerTopic)
        }
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/cleanWordTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `test consume words`() {
        val messageWithWords = getTestMessage()
        template.send("lingo", objectMapper.writeValueAsString(messageWithWords))
        Thread.sleep(2000)

        val receivedWords : List<WordDTO> = wordService.findAll()

        assertThat(messageWithWords.size).isEqualTo(receivedWords.size)

        messageWithWords.forEach { wordInMessage ->
            assert(receivedWords
                    .stream()
                    .anyMatch { receivedWord -> receivedWord.name == wordInMessage.name })
        }
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/cleanWordTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `test consume duplicate words`() {
        val messageWithDuplicateWord = listOf(
                WordRequest(name = "druppel"),
                WordRequest(name="vogel"),
                WordRequest(name="vogel"))

        template.send("lingo", objectMapper.writeValueAsString(messageWithDuplicateWord))
        Thread.sleep(2000)

        val receivedWords : List<WordDTO> = wordService.findAll()

        assertThat(receivedWords.size).isEqualTo(2)

        receivedWords.forEach { receivedWord ->
            assertThat(messageWithDuplicateWord
                    .stream()
                    .anyMatch { messageWord -> messageWord.name == receivedWord.name }).isEqualTo(true)
        }
    }

    private fun getTestMessage(): List<WordRequest> {
        return objectMapper.readValue(ClassPathResource("messages/sent-words-message.json").readText())
    }
}