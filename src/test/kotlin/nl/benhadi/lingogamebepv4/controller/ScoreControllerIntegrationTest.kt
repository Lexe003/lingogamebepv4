package nl.benhadi.lingogamebepv4.controller

import io.restassured.RestAssured
import io.restassured.http.ContentType
import nl.benhadi.lingogamebepv4.common.FullWebIntegrationTest
import nl.benhadi.lingogamebepv4.game.dto.GameDTO
import nl.benhadi.lingogamebepv4.game.request.AttemptRequest
import nl.benhadi.lingogamebepv4.score.dto.ScoreDTO
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import java.util.*

class ScoreControllerIntegrationTest: FullWebIntegrationTest() {

    @Test
    fun `get score with invalid non existing game uuid`() {
        val gameUUID = UUID.randomUUID()
        RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("score/$gameUUID")
                .then()
                .statusCode(404)
                .body("message", CoreMatchers.equalTo("Game with uuid $gameUUID not found"))
    }

    @SqlGroup(
            Sql(scripts = ["classpath:common/insertWords.sql"]),
            Sql(scripts = ["classpath:common/cleanWordTable.sql", "classpath:common/cleanGameTable.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )
    @Test
    fun `get score with finished game rounds and save it twice`() {
        val gameDTO = RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("/game").`as`(GameDTO::class.java)


        for (i in 0..2) {
            val words = listOf("vogel", "konijn", "meester")
            RestAssured.given().`when`()
                    .cookie(cookieName, playerAccount.token)
                    .contentType(ContentType.JSON)
                    .body(AttemptRequest(words[i]))
                    .post("game/${gameDTO.uuid}/attempt")
                    .then()
                    .statusCode(200)
        }

        // Get score
        val scoreDTO = RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("score/${gameDTO.uuid}")
                .`as`(ScoreDTO::class.java)

        assertThat(scoreDTO.amount).isEqualTo(150)

        // Save score
        val savedScoreDTO = RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .patch("score/${gameDTO.uuid}")
                .`as`(ScoreDTO::class.java)

        assertThat(savedScoreDTO.amount).isEqualTo(150)

        // Save score again
        RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .patch("score/${gameDTO.uuid}")
                .then()
                .statusCode(422)
                .assertThat()
                .body("message", CoreMatchers.equalTo("Score has already been persisted to the database"))
    }

    @Test
    fun `save score with not existing game uuid`() {
        val gameUUID = UUID.randomUUID()
        RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .patch("score/$gameUUID")
                .then()
                .statusCode(404)
                .body("message", CoreMatchers.equalTo("Game with uuid $gameUUID not found"))
    }

    @Test
    fun `test get all scores`() {
        RestAssured.given().`when`()
                .cookie(cookieName, playerAccount.token)
                .get("score")
                .then()
                .statusCode(200)
                .assertThat()
                .body("size()", Matchers.equalTo(0))
    }
}