INSERT INTO account
VALUES ('c6f0fec8-2f06-11ea-978f-2e728ce88125', 'email@email.com',
        '$2a$10$v7DIogS9eRaIhZhLA0I3mu5tN6yNXeg5.RPUJp/UN5T1HOsRZ2yya');

INSERT INTO account_roles
VALUES ('c6f0fec8-2f06-11ea-978f-2e728ce88123', 'c6f0fec8-2f06-11ea-978f-2e728ce88125',
        (SELECT UUID FROM ROLE where NAME = 'PLAYER'));